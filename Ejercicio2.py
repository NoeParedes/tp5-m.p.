import os

def menu():
    print( " ******* MENU *********")
    print("  a) AGREGAR VEHICULOS ")
    print("  b) RESERVAR AUTOMOVIL ")
    print("  c) BUSCAR AUTOMOVIL ")
    print("  d) ORDENAR EN FORMA ASCENDENTE(A) O DESCENDENTE(D) POR MARCA")
    print("  e) ORDENAR EN FORMA ASCENDENTE(A) O DESCENDENTE(D) POR PRECIO")
    print("  f) MOSTRARA LISTA")
    print("  g) SALIR ")
    o = input("ELIJA OPCION: ")
    return o

def Dominio():
    dominio = input('Ingrese el dominio: ')
    dominio=dominio.replace(" " , "")
    while not(len(dominio)>=6 and len(dominio)<=9  ):
        print( "*ERROR* LA EL DOMINIO ESTAR ENTRE (6-9) DIGITOS Y NO DEBE TENER ESPACIOS")
        dominio = input('Ingrese el dominio nuevamente: ')
    return dominio

def Marca():
    print('Marca: R=Renault, F=Ford, C=Citroen')
    marca = input('Ingrese la marca: ')       
    while not(marca=='R'or marca=='F'or marca=='C' ):
            print( "*ERROR* LA MARCA DEBE SER R,F,C")
            marca = input('Ingrese la marca nuevamente: ')
    return marca

def Tipo():
    print('Tipo: U=Utilitario, A=Automóvil')
    tipo = input('Ingrese el tipo: ')     
    while not(tipo=='U'or tipo=='A'):
            print( "*ERROR* LA MARCA DEBE SER R,F,C")
            tipo= input('Ingrese la marca nuevamente: ')
    return tipo

def Modelo():
    print('Modelo: en el rango [2005, 2020]')
    modelo= int(input('Ingrese el modelo: '))
    while not(modelo>=2005 and modelo<=2020  ):
        print( "*ERROR* EL MODELO DEBE ESTAR ENTRE (2005-2020)")
        modelo= int(input('Ingrese el modelo: '))
    return modelo

def Kilometraje():
    kilometraje= int(input('Ingrese el kilometraje: '))
    while not(kilometraje>=0):
        print( "*ERROR* EL KILOMETRAJE DEBE SER MAYOR A 0")
        kilometraje= int(input('Ingrese el kilometraje: '))
    return kilometraje

def PrecioValuado():
    preciovaluado= int(input('Ingrese el precio valuado: '))
    while not(preciovaluado>=10000):
        print( "*ERROR* EL PRECIO VALUADO DEBE SER MAYOR A 10000")
        preciovaluado= int(input('Ingrese el precio valuado: '))
    return preciovaluado

def PrecioVenta():
    precioventa= int(input('Ingrese el precio valuado: '))
    while not(precioventa>=0):
        print( "*ERROR* EL PRECIO VALUADO DEBE SER MAYOR A 10000")
        precioventa= int(input('Ingrese el precio valuado: '))
    return precioventa

def Estado():
    print('Estado: (V=Vendido, D=Disponible, R=Reservado).')
    estado= 'D'
    if(estado=='D'): 
        print('Disponible')     
    return estado

def VerificarDominio(lista,dominio):
    v=False
    for item in lista:
        if item[0]==dominio:
            v=True
            break
    return v         
   
def Agregar(lista): 
    lista=[['AAA001','R','A',2005,20000,30000,100000,'D'],
           ['AAA002','C','A',2006,30000,20000,150000,'D'],
           ['AAA003','F','U',2008,50000,10000,200000,'D'],
           ['AAA004','R','A',2010,23000,12000,100000,'D'],
           ['AAA005','F','U',2012,28000,15000,300000,'D'],
           ['AAA006','C','A',2005,12000,30000,130000,'D'],
           ['BAA007','C','A',2005,32000,30000,155000,'D'],
           ['AAA008','C','U',2020,17000,23000,400000,'D'],
           ['AAA009','F','A',2018,29000,25000,300000,'D'],
           ['AAA010','R','U',2015,40000,30000,250000,'D']
           ]
    c='s'
    while (c=='s' or c=='S'):
        dominio=Dominio()
        if VerificarDominio(lista,dominio)==False:
            marca=Marca()
            tipo=Tipo()
            modelo=Modelo()
            kilometraje=Kilometraje()
            preciovaluado=PrecioValuado()
            precioventa=int(preciovaluado +preciovaluado*10/100)
            estado=Estado()
            lista.append([dominio,marca,tipo,modelo,kilometraje,preciovaluado,precioventa,estado]) 
            print("Agregado")        
        else :
            print("Dominio ya existe") 
        c=input('¿Desea seguir ingresando autos? s/n:  ') 
    return lista

def mostrar(lista1):
    print('Listado de Automoviles')
    for item in lista1:
        print(item)   

def Modificar(lista):
    dominio=input('Ingrese el dominio: ')  
    for item in lista:
        if item[0]==dominio:
            item[7]='R'
            print('Modificado correctamente')
        else:
            print('El dominio ingresado no existe')

def Buscar(lista):
    dominio=input('Ingrese el dominio: ')   
    pos=-1
    for (i,item) in enumerate (lista):
       if item[0]==dominio:
            pos=i
            print(lista[pos]) 
            break
    return pos              

def IndexMarca(i):
    return i[1]

def OrdenarAscendente(lista):
    lista.sort(key=IndexMarca)
    print(lista)        
    return lista   

def OrdenarDescendente(lista):
    lista.sort(key=IndexMarca,reverse=True)
    print(lista)        
    return lista   

def OrdenarMarca(lista):
    forma=input('Eliga la forma en la que quiere ordenar A(Ascendete) o D(Descendente):')
    if forma == 'A':
        OrdenarAscendente(lista)
    elif forma == 'D':
        OrdenarDescendente(lista)
    else:
        print('Opcion ingresada incorrecta')    
    return forma

def IndexPrecio(i):
    return i[5] 
       
def OrdenarAscendente1(lista):
    lista.sort(key=IndexPrecio)
    print(lista)        
    return lista   

def OrdenarDescendente1(lista):
    lista.sort(key=IndexPrecio,reverse=True)
    print(lista)        
    return lista   

def OrdenarPrecio(lista):
    forma=input('Eliga la forma en la que quiere ordenar A(Ascendete) o D(Descendente):')
    if forma == 'A' :
        OrdenarAscendente1(lista)
    elif forma == 'D':
        OrdenarDescendente1(lista)
    else:
        print('Opcion ingresada incorrecta')    
    return forma

# PRINCIPAL
opcion = ''
lista=[]
while (opcion != 'g'):
    os.system("cls")
    opcion = menu()
    if opcion == 'a':
        lista=Agregar(lista) 
    elif opcion == 'b':
        Modificar(lista)
    elif opcion == 'c':
        Buscar(lista)   
    elif opcion == 'd':
        OrdenarMarca(lista)
    elif opcion == 'e':
        OrdenarPrecio(lista)
    elif opcion == 'f':
        mostrar(lista)
    elif opcion == 'g':
       print('Fin del programa')
    else:
        print("ERROR TIENE QUE ELEGIR UNA OPCION QUE ESTE EN EL MENU ")
    input (" Presione cualquier letra ")
    